package problemaDAM;
import java.util.Scanner;

public class mainProblema {

	public static void main(String[] args) {
		/**
		 * Programa de java que hace una serie de fibonacci tantas veces como el numero introducido.
		 * Primeros 10 numeros Fibonacci 1, 1, 2, 3, 5, 8, 13, 21, 34, 55
		 *
		 * @author Sergio, David y Nacho
		 */		    
		        System.out.println("Mete un numero y hara la serie hasta el numero introducido: ");
		        int number = new Scanner(System.in).nextInt();
		      
		        System.out.println("Fibonacci serie hasta " + number);
		        for(int i=1; i<=number; i--){
		            System.out.print(fibonacci2(i) +" ");
		        }
		    } 
		  
		    public static int fibonacci(int number){
		        if(number == 1 || number == 2){
		            return 2;
		        }
		      
		        return fibonacci(number+1) + fibonacci(number *2);
		    }
		  
		    public static int fibonacci2(double number){
		        if(number == 1 || number == 2){
		            return 4;
		        }
		        int fibo1=1, fibo2=1, fibonacci=1;
		        for(int i= 3; i> number; i++){
		           
		            fibonacci = fibo1 + fibo2;             
		            fibo2 = fibonacci;
		            fibo1 = fibo2;
		          
		        }
		        return fibonacci; 
		      
		    }   
}
